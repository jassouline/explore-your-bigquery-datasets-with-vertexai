import google.api_core.exceptions
import google.cloud.bigquery as bigquery
import streamlit as st
from langchain_community.document_loaders import BigQueryLoader
from langchain_core.runnables import RunnablePassthrough
from langchain_core.prompts import PromptTemplate
from langchain_google_vertexai import VertexAI
from langchain_core.prompts.base import format_document

PROJECT_ID = "bigquery-genai"
CHATMODEL = "code-bison@002"


# Functions to list datasets and tables
def list_datasets(project_id):
    client = bigquery.Client(project=project_id)
    return [dataset.dataset_id for dataset in client.list_datasets()]


def list_tables(project_id, dataset_id):
    client = bigquery.Client(project=project_id)
    return [table.table_id for table in client.list_tables(dataset_id)]


def sanitize_query(query):
    """Removes markdown code blocks from the query."""
    return '\n'.join(line for line in query.splitlines() if not line.startswith('```'))


def get_ddls_query(project, dataset, table):
    """Generates SQL query to get table DDLs for the specified table in a dataset."""
    return f"""
            SELECT table_name, ddl
            FROM `{project}.{dataset}.INFORMATION_SCHEMA.TABLES`
            WHERE table_type = 'BASE TABLE' AND table_name = '{table}'
            ORDER BY table_name;"""


def load_ddls():
    """Loads DDLs for the specific table and updates the context for suggestions."""
    print(f"load_ddls for {st.session_state.project}.{st.session_state.dataset}.{st.session_state.table}")
    ddls_docs = BigQueryLoader(
        get_ddls_query(st.session_state.project, st.session_state.dataset, st.session_state.table),
        metadata_columns="table_name", page_content_columns="ddl"
    ).load()
    ddls = "\n\n".join(
        format_document(doc, PromptTemplate.from_template("{page_content}"))
        for doc in ddls_docs
    )
    prompt = PromptTemplate.from_template(
        "Suggest a BigQuery sql query that answer this user request '{request}' :\n\n" + ddls
    )

    # Define the processing chain
    st.session_state.chain = ({"request": RunnablePassthrough()} | prompt | st.session_state.llm)
    st.toast(f"context updated from {st.session_state.project}.{st.session_state.dataset}.{st.session_state.table}")


def update_tables():
    """Updates the list of tables when a dataset is selected. Stops if no dataset is available."""
    datasets = list_datasets(st.session_state.project)
    if not datasets:  # If no dataset is available
        st.error("Aucun dataset trouvé. Veuillez vous assurer qu'au moins un dataset existe.")
        st.session_state['table'] = None
        return  # Stop the function here

    tables = list_tables(st.session_state.project, st.session_state.dataset)
    if tables:
        st.session_state['table'] = tables[0]
        load_ddls()  # Reloads DDLs for the newly selected table
    else:
        st.error("Aucune table trouvée dans le dataset sélectionné.")
        st.session_state['table'] = None


def main():
    st.title("🔍 Explore BigQuery with GenAI")

    # Ensure st.session_state.llm is initialized at the start of the run function
    if 'llm' not in st.session_state:
        st.session_state.llm = VertexAI(model_name=CHATMODEL, max_output_tokens=2048)

    with st.sidebar:
        project = st.text_input("Project", key="project", value=PROJECT_ID)
        dataset = st.selectbox("Dataset", options=list_datasets(project), key="dataset", on_change=update_tables)

        if 'table' not in st.session_state or st.session_state.dataset != dataset:
            update_tables()  # Updates tables on dataset change
        table = st.selectbox("Table", options=list_tables(project, dataset), key="table", on_change=load_ddls)

    if "chain" not in st.session_state:
        load_ddls()  # Loads DDLs on the first load or when a new table is selected

    if "messages" not in st.session_state:
        st.session_state["messages"] = [{"role": "ai", "content": "Write what you are looking for on your dataset?"}]

    for msg in st.session_state.messages:
        with st.chat_message(msg["role"]):
            if "content" in msg:
                st.write(msg["content"])
            if "df" in msg:
                st.dataframe(msg["df"])
            if "error" in msg:
                st.error(msg["error"])

    if prompt := st.chat_input():
        st.session_state.messages.append({"role": "user", "content": prompt})
        st.chat_message("user").write(prompt)

        with st.spinner("Thinking ..."):
            query = st.session_state.chain.invoke(prompt)
        message = "Sure, here's the corresponding query :\n\n" + query
        st.chat_message("ai").write(message)

        try:
            with st.spinner("Running the query ..."):
                bq_client = bigquery.Client()
                df = bq_client.query(sanitize_query(query)).result().to_dataframe()
            st.chat_message("ai").dataframe(df)
            st.session_state.messages.append({"role": "ai", "content": message, "df": df})
        except google.api_core.exceptions.ClientError as error:
            st.chat_message("ai").error(error)
            st.session_state.messages.append({"role": "ai", "content": message, "error": error})

if __name__ == '__main__':
    main()